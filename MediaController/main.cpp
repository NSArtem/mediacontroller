//
//  main.cpp
//  MediaController
//
//  Created by Artem Abramov on 9/6/13.
//  Copyright (c) 2013 Artem Abramov. All rights reserved.
//

#include <iostream>
#include "PltMicroMediaController.h"
#include "PltFileMediaServer.h"
#include "PltMediaRenderer.h"
#include "PltXbox360.h"
#include "PltVersion.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

//#define HAS_RENDERER 1
#define HAS_SERVER 1
//#define SIMULATE_XBOX_360 1
//#define SIMULATE_PS3 1
//#define BROADCAST_EXTRA 1

int main(int argc, const char * argv[])
{
    // setup Neptune logging
//    NPT_LogManager::GetDefault().Configure("plist:.level=FINE;.handlers=ConsoleHandler;.ConsoleHandler.colors=off;.ConsoleHandler.filter=24");
    
    // Create upnp engine
    PLT_UPnP upnp;
    
    // Create control point
    PLT_CtrlPointReference ctrlPoint(new PLT_CtrlPoint());
    
    // Create controller
    PLT_MicroMediaController controller(ctrlPoint);
    
#ifdef HAS_SERVER
    // create device
    PLT_DeviceHostReference server(
                                   new PLT_FileMediaServer("/Users/aabramov/Documents/AudioFileTests",
                                                           "Platinum UPnP Media Server"));
    
    server->m_ModelDescription = "MERA NN UPnP TEST";
    server->m_ModelURL = "http://www.plutinosoft.com/";
    server->m_ModelNumber = "1.0";
    server->m_ModelName = "Platinum File Media Server";
    server->m_Manufacturer = "Plutinosoft";
    server->m_ManufacturerURL = "http://www.plutinosoft.com/";
    
    // add device
    upnp.AddDevice(server);
    
    // remove device uuid from ctrlpoint
//    ctrlPoint->IgnoreUUID(server->GetUUID());
#endif
    
    // add control point to upnp engine and start it
    upnp.AddCtrlPoint(ctrlPoint);
    upnp.Start();
    
#ifdef BROADCAST_EXTRA
    // tell control point to perform extra broadcast discover every 6 secs
    // in case our device doesn't support multicast
    ctrlPoint->Discover(NPT_HttpUrl("255.255.255.255", 1900, "*"), "upnp:rootdevice", 1, 6000);
    ctrlPoint->Discover(NPT_HttpUrl("239.255.255.250", 1900, "*"), "upnp:rootdevice", 1, 6000);
#endif
    
#ifdef SIMULATE_XBOX_360
    // override default headers
    NPT_HttpClient::m_UserAgentHeader = "Xbox/2.0.8955.0 UPnP/1.0 Xbox/2.0.8955.0";
    NPT_HttpServer::m_ServerHeader    = "Xbox/2.0.8955.0 UPnP/1.0 Xbox/2.0.8955.0";
    
    // create device
    PLT_DeviceHostReference xbox(new PLT_Xbox360("30848576-1775-2000-0000-00125a8fefad"));
    xbox->SetByeByeFirst(false);
    xbox->m_SerialNumber = "308485761776";
    
    // add device
    upnp.AddDevice(xbox);
    ctrlPoint->IgnoreUUID(xbox->GetUUID());
    
    // xbox issues a search for the content directory service
    // 10 secs after announcing itself to make sure
    // it got detected and inspected first
    
    ctrlPoint->Search(
                      NPT_HttpUrl("239.255.255.250", 1900, "*"),
                      "urn:schemas-microsoft-com:service:MSContentDirectory:1", 2, 10000, NPT_TimeInterval(10, 0));
    ctrlPoint->Search(
                      NPT_HttpUrl("239.255.255.250", 1900, "*"),
                      "urn:schemas-upnp-org:service:ContentDirectory:1", 2, 10000, NPT_TimeInterval(10, 0));
    
#endif
    
    // start to process commands 
    controller.ProcessCommandLoop();
    
    // stop everything
    upnp.Stop();
    
    return 0;
}

